% offset- �ndice inicial com dados na matriz expandida
% y - tamanho da matriz expandida no eixo y
% x - tamanho da matriz expandida no eixo x
% label - matriz com etiquetas criadas na primeira varredura
% representativeLabel - Mapeamento da etiqueta intermedi�ria para a
% etiqueta final
% label - retorno da matriz de etiquetas depois do merge
% pixelArea - Informa��es com a �rea dos componentes conexos
%
function [label, pixelArea] = secondScanLabelMerge(offset, y, x, label, representativeLabel)
    
    [yPixelArea, xPixelArea] = size(representativeLabel);
    pixelArea = zeros(yPixelArea, xPixelArea);
    for n = offset:y
        for m = offset:x
            if (label(n, m) ~= 0)
                label(n,m) = representativeLabel(label(n, m));
                pixelArea(label(n,m)) = pixelArea(label(n,m)) + 1;
            end
        end
    end
end