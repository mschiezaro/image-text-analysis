function imageOut = close(imageIn,structuredElement, yStructuredElementCenter, xStructuredElementCenter, imageOutPath)

    dilated = dilate(imageIn, structuredElement, yStructuredElementCenter, xStructuredElementCenter, 0);
    imageOut = erode(dilated, structuredElement, yStructuredElementCenter, xStructuredElementCenter, 0);
    
    if imageOutPath
        imwrite(~imageOut, imageOutPath);
    end
end