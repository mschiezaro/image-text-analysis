function [imageOut, x1, x2, y1, y2] = boundingBox(imageIn, imageLabel, greatestLabelFounded, imageOutPath) 

    [y,x] = size(imageLabel);
    imageOut = imageIn;
    x1 = zeros(1, greatestLabelFounded);
    x2 = zeros(1, greatestLabelFounded);
    y1 = zeros(1, greatestLabelFounded);
    y2 = zeros(1, greatestLabelFounded);
    for i = 1:greatestLabelFounded
        x1(1, i) = x;
        y1(1, i) = y;
    end
    for n = 1:y
        for m =1:x
            label = imageLabel(n, m);
            if label ~= 0

                if x1(1, label) > m 
                    x1(1, label) = m ;
                end
                if y1(label) > n 
                    y1(label) = n ;
                end
                if x2(1, label) < m 
                    x2(1, label) = m ;
                end
                if y2(1, label) < n 
                    y2(1, label) = n ;
                end
            end

        end
    end
    for j = 1:greatestLabelFounded
        
        x1Bound = x1(1, j) - 1;
        x2Bound = x2(1, j) + 1;
        y1Bound = y1(1, j) - 1;
        y2Bound = y2(1, j) + 1;
        
        for row = x1Bound:x2Bound
            if row > 0 && row <= x &&  y1Bound > 0 
                imageOut(y1Bound, row) = 1;
            end
        end
        for row = x1Bound:x2Bound
            if row > 0 && row <= x &&  y2Bound <= y 
                imageOut(y2Bound, row) = 1;
            end
        end
        for col = y1Bound:y2Bound
            if col > 0 && col <= y && x1Bound > 0 
                imageOut(col, x1Bound) = 1;
            end
        end
        for col = y1Bound:y2Bound
            if col >= 0 && col <= y && x2Bound <= x 
                imageOut(col,x2Bound) = 1;
            end
        end
    end
    if imageOutPath
        imwrite(~imageOut, imageOutPath);
    end

end
