% offset - início de dados na matriz expandida
% y - tamanho da matriz expandida no eixo y
% x - tamanho da matriz expandida no eixo x
% image - imagem na matriz expandida
% label - matriz com as etiquetas
% provisionalLabel - Mapeia uma etiqueta final para um conjunto de
% etiquetas intermediárias
% representativeLabel Mapeia uma etiqueta intermediária para a sua final
%
function [label, provisionalLabel, representativeLabel] = firstScanLabel(offset, y, x, image)

    label = zeros(y, x); 
    currentLabel = 1;
    provisionalLabel = {}; 
    representativeLabel = []; 
    black = 1; 

    for n = offset:y 
        
        for m = offset:x
            
            if (image(n, m) == black)
                % pixel a ser analisado
                currentPixel = image(n, m); 
                % pixel acima
                northPixel = image(n-1,m);
                % pixel a esquerda
                westPixel = image(n, m-1);
                
                if (currentPixel == westPixel)  
                    % se for equivalente recebe mesmo label
                    label(n,m) = label(n, m-1);
                    
                    if (currentPixel == northPixel) 
                        
                        rLabelNorth = representativeLabel(label(n-1, m));
                        rLabelWest =  representativeLabel(label(n, m-1));
                        pLabelWest = provisionalLabel{rLabelWest};
                        pLabelNorth = provisionalLabel{rLabelNorth};
                     
                       	if(rLabelWest  < rLabelNorth ) 
                            % atribue a equivalencia das etiquetas
                            [yN, xN] = size(pLabelNorth);
                            for (i = 1:xN) 
                                representativeLabel(pLabelNorth(i)) = rLabelWest;
                            end
                        	provisionalLabel{rLabelWest} = union(pLabelWest, pLabelNorth);
                        	provisionalLabel{rLabelNorth} = [];
                            
                        else
                            if ( rLabelWest > rLabelNorth ) 
                                % atribue a equivalencia das etiquetas
                                [yW, xW] = size(pLabelWest);
                                for (j = 1:xW) 
                        			representativeLabel(pLabelWest(j)) = rLabelNorth;
                                end
                        		provisionalLabel{rLabelNorth} = union(pLabelNorth, pLabelWest);
                        		provisionalLabel{rLabelWest} = [];
                                
                            end
                        end
                    end
                else 
                    
                    if (currentPixel == northPixel)
                            label(n, m) = label(n-1, m);
                    else
                        % cria um novo label
                        label(n, m) = currentLabel;
                        representativeLabel(label(n, m)) = label(n, m);
                        provisionalLabel{label(n, m)} = [label(n, m)];
                        currentLabel = currentLabel + 1;
                    end
                end
            end
        end
    end