function nrWords = countWords(x1, x2, y1, y2)
    nrWords = 0 ;
    [n,m] = size(x1);
    for i = 1:m
        initX = x1(n, i);
        endX = x2(n, i);
        initY = y1(n, i);
        endY = y2(n, i);         
        if initX ~= 0 && endX ~= 0 && initY ~= 0 && endY ~= 0 
            area = (endX - initX + 1) * (endY - initY + 1);
            if area > 25
                nrWords = nrWords + 1 ;
            end
        end
    end
end
