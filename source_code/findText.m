function [imageOut,  nrLines] = findText(imageIn, x1, x2, y1, y2, blackPixelByTotalArea, horizontalTransitionBlackPixel, verticalTransitionBlackPixel, imageOutPath)

    [n, m] = size(imageIn);
    imageOut = zeros(n, m);
    [n, m] = size(x1);
    nrLines = 0 ;
    for i = 1:m
        if blackPixelByTotalArea(n,i) < 0.95 && verticalTransitionBlackPixel(n, i) < 0.06 && horizontalTransitionBlackPixel(n, i) < 0.03
            initX = x1(n, i);
            endX = x2(n, i);
            initY = y1(n, i);
            endY = y2(n, i);         
            if initX ~= 0 && endX ~= 0 && initY ~= 0 && endY ~= 0             
                nrLines = nrLines + 1 ;
                for y = initY:endY
                    for x = initX:endX          
                        imageOut(y, x) = imageIn(y, x);
                    end
                end
            end
        end
    end
    if imageOutPath
        imwrite(~imageOut, imageOutPath);
    end
end