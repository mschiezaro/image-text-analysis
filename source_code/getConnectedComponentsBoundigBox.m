function [connectedComponents, imageOut, x1, x2, y1, y2] = getConnectedComponentsBoundigBox(originalImage, segImageIn, imageOutPath) 
    % nr de colunas e linhas que a imagem original ser� expandida
    additionalSize = 1 ; 
    % le a imagem 
    [offset, yOriginalSize, xOriginalSize, expandedImage] = readImage2(additionalSize, segImageIn);
    % recupera o tamanho da imagem
    [yExpanded, xExpanded] = size(expandedImage); 
    % faz a primeira varredura e cria as etiquetas 
    [label, provisionalLabel, representativeLabel] = firstScanLabel(offset, yExpanded, xExpanded, expandedImage);
    % faz o merge e acumula os valores de n�mero de pixels por componente
    % conexo
    [label, pixelArea] = secondScanLabelMerge(offset, yExpanded, xExpanded, label, representativeLabel);
    % recupera a quantidade total de componentes conexos encontrados
    connectedComponents = getConnectedComponentQtd(provisionalLabel);
    % encontra o maior label
    maxLabel = max(max(label));
    % desenha um quadrado em todos os componentes conexos encontrados
    label = label(2:yOriginalSize+1, 2:xOriginalSize+1);
    [imageOut, x1, x2, y1, y2] = boundingBox(originalImage, label, maxLabel, imageOutPath);
end