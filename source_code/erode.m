function imageOut = erode(imageIn,structuredElement, yStructuredElementCenter, xStructuredElementCenter, imageOutPath)

    [nIm, mIm] = size(imageIn);
    [nSE, mSE] = size(structuredElement);    
    imageOut = zeros(nIm, mIm);
    black = 1;
    first = 1;
    
    workImage = ones(nIm + 4*nSE, mIm + 4*mSE);
    workImageOut = ones(nIm + 4*nSE, mIm + 4*mSE);
    imageIntermediate = ones(nIm + 4*nSE, mIm + 4*mSE);
    
    initX = 2*mSE+1;
    endX = mIm+2*mSE;
    initY = 2*nSE+1;
    endY = nIm+2*nSE;
    workImage(initY:endY, initX: endX) = imageIn;
    workImageOut(initY:endY, initX: endX) = zeros(nIm, mIm);
    imageIntermediate(initY:endY, initX: endX) = zeros(nIm, mIm);

    structutedElementTranslationPositionMapX = zeros(mSE);
    structutedElementTranslationPositionMapY = zeros(nSE);
    
    for xSE = 1:mSE
        structutedElementTranslationPositionMapX(xSE) = xSE - xStructuredElementCenter ;
    end
    for ySE = 1:nSE
        structutedElementTranslationPositionMapY(ySE) = ySE - yStructuredElementCenter ;
    end

    for ySE = 1:nSE
        for xSE = 1:mSE
            if structuredElement(ySE,xSE) == black
                endIndexY = endY + nSE;
                endIndexX = endX + mSE;
                for yIm = initY:endIndexY
                    for xIm = initX:endIndexX
                        if workImage(yIm,xIm) == black
                            yDilated = yIm - structutedElementTranslationPositionMapY(ySE);
                            xDilated = xIm - structutedElementTranslationPositionMapX(xSE);                            
                            imageIntermediate(yDilated, xDilated) = 1;
                        end
                    end
                end
            end
            if first
                workImageOut = imageIntermediate;
                first = 0;
            else
                workImageOut = and(workImageOut, imageIntermediate);
            end
            imageIntermediate = ones(nIm + 4*nSE, mIm + 4*mSE);            
            imageIntermediate(initY:endY, initX: endX) = zeros(nIm, mIm);            
        end
    end
    imageOut = workImageOut(initY:endY, initX: endX);
    if imageOutPath
        imwrite(~imageOut, imageOutPath);
    end
end
