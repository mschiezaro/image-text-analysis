function imageOut = dilate(imageIn,structuredElement, yStructuredElementCenter, xStructuredElementCenter, imageOutPath)

    [nIm, mIm] = size(imageIn);
    imageOut = zeros(nIm, mIm);
    [nSE, mSE] = size(structuredElement);    
    imageOut = zeros(nIm, mIm);
    black = 1;
    
    workImage = ones(nIm + 4*nSE, mIm + 4*mSE);
    workImageOut = zeros(nIm + 4*nSE, mIm + 4*mSE);
    initX = 2*mSE+1;
    endX = mIm+2*mSE;
    initY = 2*nSE+1;
    endY = nIm+2*nSE;
    workImage(initY:endY, initX: endX) = imageIn;
    structutedElementTranslationPositionMapX = zeros(mSE);
    structutedElementTranslationPositionMapY = zeros(nSE);
    
    for xSE = 1:mSE
        structutedElementTranslationPositionMapX(xSE) = xSE - xStructuredElementCenter ;
    end
    for ySE = 1:nSE
        structutedElementTranslationPositionMapY(ySE) = ySE - yStructuredElementCenter ;
    end
    
    for ySE = 1:nSE
        for xSE = 1:mSE
            if structuredElement(ySE,xSE) == black
                endIndexY = endY + nSE;
                endIndexX = endX + mSE;

                for yIm = initY:endIndexY
                    for xIm = initX:endIndexX
                        if workImage(yIm,xIm) == black
                            yDilated = yIm + structutedElementTranslationPositionMapY(ySE) ;
                            xDilated = xIm + structutedElementTranslationPositionMapX(xSE) ;                            
                            workImageOut(yDilated, xDilated) = 1;
                        end
                    end
                end
            end
        end
    end
    imageOut = workImageOut(initY:endY, initX: endX);
    if imageOutPath
        imwrite(~imageOut, imageOutPath);
    end
end
