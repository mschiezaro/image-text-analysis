function [horizontalTransitionBlackPixel, verticalTransitionBlackPixel] = findTransitionBlackPixel(imageIn, x1, x2 , y1, y2)

    [n,m] = size(x1);
    reason = zeros(n,m);
    horizontalTransition = zeros(n,m);
    verticalTransition = zeros(n,m);
    horizontalTransitionBlackPixel = zeros(n,m);
    verticalTransitionBlackPixel = zeros(n,m);
    
    for i=1:m
        initX = x1(n, i);
        endX = x2(n, i);
        initY = y1(n, i);
        endY = y2(n, i);    

        if initX ~= 0 && endX ~= 0 && initY ~= 0 && endY ~= 0 
            
            nrBlack = 0;
            nrBlack = 0;        
            black = 1;
            white = 0;
        
            for y = initY:endY
                lastPixel = black;
                for x = initX:endX            
                    if imageIn(y, x) == black
                        nrBlack = nrBlack + 1 ;
                        if lastPixel == white
                            horizontalTransition(n, i) = horizontalTransition(n, i) + 1;
                        end
                    end
                    lastPixel = imageIn(y, x);                    
                end
            end

        
            for x = initX:endX     
                lastPixel = black;
                for y = initY:endY                 
                    if imageIn(y, x) == black
                        if lastPixel == white
                            verticalTransition(n, i) = verticalTransition(n, i) + 1;
                        end
                    end
                    lastPixel = imageIn(y, x);                                    
                end
            end
            horizontalTransitionBlackPixel(n,i) = horizontalTransition(n,i) / nrBlack ;
            verticalTransitionBlackPixel(n,i) = verticalTransition(n,i) / nrBlack ;
        else
            horizontalTransitionBlackPixel(n,i) = 0;
            verticalTransitionBlackPixel(n,i) = 0;
        end
    end
end
