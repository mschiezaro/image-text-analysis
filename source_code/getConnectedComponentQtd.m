% Recupera o n�mero de componentes conexos
function connectedComponents = getConnectedComponentQtd(provisionalLabel)
    [y,x] = size(provisionalLabel);
    connectedComponents = 0;
    for m = 1:x
        if(~isempty(provisionalLabel{m}))
            connectedComponents = connectedComponents + 1;
        end
    end
end