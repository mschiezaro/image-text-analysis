% le a imagem e retorma as informaçoes da imagem representada em uma matriz
% expandida
function [offset, yOriginalSize, xOriginalSize, expandedImage] = readImage2(additionalSize, image)
    
    [yOriginalSize, xOriginalSize] = size(image);      
    offset = additionalSize + 1;
    expandedImage = zeros(yOriginalSize + additionalSize , xOriginalSize + additionalSize); 
    yExpanded = yOriginalSize + additionalSize; 
    xExpanded = xOriginalSize + additionalSize; 
    expandedImage(offset:yExpanded,offset:xExpanded) = image;
    
end