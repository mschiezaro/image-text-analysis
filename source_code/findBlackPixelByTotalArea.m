function blackPixelByTotalArea = findBlackPixelByTotalArea(imageIn, x1, x2 , y1, y2)

    [n,m] = size(x1);
    blackPixelByTotalArea = zeros(n,m);
    
    for i=1:m

        initX = x1(n, i);
        endX = x2(n, i);
        initY = y1(n, i);
        endY = y2(n, i);        
        
        if initX ~= 0 && endX ~= 0 && initY ~= 0 && endY ~= 0 
            area = (endX - initX + 1) * (endY - initY + 1);
            nrBlack = 0;
            black = 1;
            for y = initY:endY
                for x = initX:endX            
                    if imageIn(y, x) == black
                        nrBlack = nrBlack + 1 ;
                    end
                end
            end
            blackPixelByTotalArea(i) = nrBlack / area ;
        else
            blackPixelByTotalArea(i) = 0;
        end
    end
end
