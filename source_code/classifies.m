function [nrLines, words, connectedComponents] = classifies(imageInPath, imageOutDirectory)

    imageIn = ~imread(imageInPath);
    [y,x] = size(imageIn);
    
    structuredElement = ones(1, 100);
    % passo 1
    dilated_1_100 = dilate(imageIn, structuredElement, 1, 1, strcat(imageOutDirectory,'dilated_step_1.pbm'));
    % passo 2
    eroded_1_100 = erode(dilated_1_100, structuredElement, 1, 1, strcat(imageOutDirectory,'eroded_step_2.pbm'));

    structuredElement = ones(200, 1);
    % passo 3
    dilated_200_1 = dilate(imageIn, structuredElement, 1, 1, strcat(imageOutDirectory,'dilated_step_3.pbm'));
    % passo 4
    eroded_200_1 = erode(dilated_200_1, structuredElement, 1, 1, strcat(imageOutDirectory,'eroded_step_4.pbm'));
    % passo 5
    andImage = and(eroded_1_100, eroded_200_1);
    imwrite(~andImage, strcat(imageOutDirectory,'and_step_5.pbm'));        
    
    structuredElement = ones(1, 30);
    % passo 6    
    close_1_30 = close(andImage, structuredElement, 1, 1, strcat(imageOutDirectory,'close_step_6.pbm'));
    
    % passo 7    
    [connectedComponents, imageOut, x1, x2, y1, y2] = getConnectedComponentsBoundigBox(imageIn, close_1_30,  strcat(imageOutDirectory,'bounding_box_step_7_e_8.pbm')) ;
    
    % passo 8    
    blackPixelByTotalArea = findBlackPixelByTotalArea(close_1_30, x1, x2 , y1, y2);
    [horizontalTransitionBlackPixel, verticalTransitionBlackPixel] = findTransitionBlackPixel(close_1_30, x1, x2 , y1, y2);
    
    % passo 9        
    [imageText,  nrLines] = findText(imageIn, x1, x2, y1, y2, blackPixelByTotalArea, horizontalTransitionBlackPixel, verticalTransitionBlackPixel, strcat(imageOutDirectory,'only_text_step_9.pbm'));
    findDraws(imageIn, x1, x2, y1, y2, blackPixelByTotalArea, horizontalTransitionBlackPixel, verticalTransitionBlackPixel, strcat(imageOutDirectory,'only_draws_step_9.pbm'));

    structuredElement = ones(8,12);
    % passo 10
    close_1_11 = close(imageText, structuredElement, 4, 1, strcat(imageOutDirectory,'close_step_10.pbm'));
    [connected, x1, x2, y1, y2] = getConnectedComponentsBoundigBox(imageIn, close_1_11, strcat(imageOutDirectory,'bounding_box_step_10.pbm')) ;
    words = countWords(x1, x2, y1, y2);
    
end
 